# Implementing a Scheduler/Timer in C++ using POSIX Threads
A Timer or Scheduler is an object that exececutes a given task `N` times with a given `Period` and initial `Delay`

## Code Examples
Simple Example
```c
#include "Concurrency/Scheduler.hpp"

void foo( void );

int main( void ) {
    int threadPoolSize;
    int workPoolSize;

    //Initialize the scheduler
    AUTH::Scheduler scheduler(workPoolSize, threadPoolSize);

    int N; //Number of executions
    int P; //Period
    int D; //Initial delay

    //Create a task
    AUTH::Scheduler::Task task(foo, N, P, D);
    //Submit the task
    scheduler.submit(task);

    //Wait to finish the task execution
    scheduler.wait();
}
```

More advanced control
```c
#include "Concurrency/Scheduler.hpp"

void foo  ( void ); //Executed N times
void err  ( void ); //Executed if the workQueue is full
void stop ( void ); //Executed when the last foo is executed
void start( void ); //Executed before any foo is executed

int main( void ) {
    int threadPoolSize;
    int workPoolSize;

    //Initialize the scheduler
    AUTH::Scheduler scheduler(workPoolSize, threadPoolSize);

    int N; //Number of executions
    int P; //Period
    int D; //Initial delay

    //Create a task
    AUTH::Scheduler::Task task(foo, start, stop, err, N, P, D);
    //Submit the task
    scheduler.submit(task);

    //Wait to finish the task execution
    scheduler.wait();
}
```

## Limitations
The scheduler does not store a copy of the task itself and is only accesing it by reference, so make sure that its not out of scope when executing with the scheduler.

### Example
```c
int main( void ) {

    //....

    //Initialize the scheduler
    AUTH::Scheduler scheduler(workPoolSize, threadPoolSize);

    //Temporary scope
    {
        //Create a task
        AUTH::Scheduler::Task task(foo, start, stop, err, N, P, D);
        //Submit the task
        scheduler.submit(task);
        //...
        //Task dies at this point
    }

    //Error Task does not exist any more

    //Wait to finish the task execution
    scheduler.wait();
}
```

## Compilation
Place in the [src](./src) directory a source file and execute:
```
$ make
```

### Debug vs Execution mode
To disable debug mode change the [makefile](./makefile)

From:
```makefile
CFLAGS := -I$(INC_DIR) -g3
```

To:
```makefile
CFLAGS := -I$(INC_DIR) -O3 -D NDEBUG
```
