#include "Concurrency/Scheduler.hpp"
#include "Stopwatch.hpp"
#include <unistd.h>

static const std::function<void()> NOP = [](){};

AUTH::Scheduler::Scheduler(Size const queueSize, Size const poolSize)
    : taskQueue(queueSize), terminated(false)
{
    for(Size i = 0; i < poolSize; ++i)
        this->consumers.emplace_back(this);
}

auto AUTH::Scheduler::submit(const Task& task) -> void {
    producers.emplace_back(this, task);
}

auto AUTH::Scheduler::wait() -> void {
    this->producers.clear();
    this->terminated = true;
    //TODO: bug when consumer.size() < pool.size()
    for(int i = 0; i < this->consumers.size(); ++i)
        this->taskQueue.offer(::NOP);
    this->consumers.clear();
}

AUTH::Scheduler::Task::Task(const Routine& timer, Size const num, Time const period, Time const delay)
    : Task(timer, ::NOP, ::NOP, ::NOP, num, period, delay) {}

AUTH::Scheduler::Task::Task(const Routine& timer, const Routine& start, const Routine& stop, const Routine& err, Size const num, Time const period, Time const delay)
    : _timer(timer), _start(start), _stop(stop), _err(err), num(num), period(period), delay(delay) {}


AUTH::Scheduler::Consumer::Consumer(Scheduler* const outer)
    : ScopedWorker(
        [=](){
            while(!outer->terminated) 
                outer->taskQueue.pop()();
        }
    ) {}

AUTH::Scheduler::Producer::Producer(Scheduler* const outer, const Task& task)
    : ScopedWorker(
        [=](){
            Mutex mutex;
            ConditionVariable barrier;

            if(task.getDelay() > 0)
                usleep(task.getDelay());
            
            task.start();

            for(Size i = 0; i < task.getNumber(); ++i) {
                Stopwatch stopwatch;
                ScopedLock lock(mutex);

                if(outer->taskQueue.offer([&](){ ScopedLock lock(mutex); task.timer(); barrier.signal(); }))
                    barrier.wait(mutex);
                else
                    task.err();

                const long period = task.getPeriod() - stopwatch.getDuration();
                if(period > 0)
                    usleep(period);
            }

            task.stop();
        }
    ) {}

auto AUTH::Scheduler::Task::timer() const -> void {
    this->_timer();
}

auto AUTH::Scheduler::Task::start() const -> void {
    this->_start();
}

auto AUTH::Scheduler::Task::stop() const -> void {
    this->_stop();
}

auto AUTH::Scheduler::Task::err() const -> void {
    this->_err();
}

auto AUTH::Scheduler::Task::getNumber() const -> Size {
    return this->num;
}

auto AUTH::Scheduler::Task::getPeriod() const -> Size {
    return this->period;
}

auto AUTH::Scheduler::Task::getDelay() const -> Size {
    return this->delay;
}
