#include "Concurrency/Thread.hpp"
#include <stdexcept>
#include "Helpers.hpp"


AUTH::Thread::Thread()
    : started(false), joined(false) {}

auto AUTH::Thread::start() -> void {
    static const auto ThisRun = [](void* self)->void*{ static_cast<Thread*>(self)->task(); return nullptr; };
    if(this->started)
        throw std::runtime_error("Thread already started");
    dynamic_err_assert(pthread_create(&this->cthread, nullptr, ThisRun, this));
    this->started = true;
}

auto AUTH::Thread::join() -> void {
    if(this->joined)
        return;
    dynamic_err_assert(pthread_join(this->cthread, nullptr));
    this->joined = true;
}

auto AUTH::Thread::isStarted() const -> bool {
    return this->started;
}

auto AUTH::Thread::isJoined() const -> bool {
    return this->joined;
}

AUTH::Worker::Worker(const Runnable& runnable)
    : runnable(runnable) {}

auto AUTH::Worker::task() const -> void {
    this->runnable();
}

AUTH::ScopedWorker::ScopedWorker(const Runnable& runnable)
    : Worker(runnable)
{
    this->start();
}

AUTH::ScopedWorker::~ScopedWorker() {
    this->join();
}