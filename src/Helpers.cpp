#include "Helpers.hpp"
#include <sstream>
#include <stdexcept>

auto AUTH::_dynamic_err_assert(int err, const char* sig, const char* file, int line) -> void {
    if(err != 0) {
        std::stringstream sstream;
        sstream << "Error code: " << err << "\nWhile executing: " << sig << "\nAt: " << file << ':' << line;
        throw std::runtime_error(sstream.str());
    }
}

auto AUTH::_dynamic_assert(int err, const char* sig, const char* file, int line) -> void {
    if(err == 0) {
        std::stringstream sstream;
        sstream << "Error While executing: " << sig << "\nAt: " << file << ':' << line;
        throw std::runtime_error(sstream.str());
    }
}