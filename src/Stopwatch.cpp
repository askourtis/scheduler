#include "Stopwatch.hpp"
#include "Helpers.hpp"
#include <sys/time.h>

auto AUTH::Stopwatch::Clock() -> Time {
    timeval time;
    dynamic_err_assert(gettimeofday(&time, nullptr));
    return time.tv_usec + 1000000L * time.tv_sec;
}

AUTH::Stopwatch::Stopwatch(bool start) {
    this->reset();
    if(start)
        this->start();
}

auto AUTH::Stopwatch::start() -> void {
    if(this->isCounting())
        return;
    this->begin = Clock();
}

auto AUTH::Stopwatch::stop() -> void {
    if(!this->isCounting())
        return;
    this->duration += Clock() - begin;
    this->begin     = 0;
}

auto AUTH::Stopwatch::isCounting() -> bool {
    return this->begin;
}

auto AUTH::Stopwatch::getDuration() -> Time {
    if(this->isCounting())
        return this->duration + (Clock() - this->begin);
    return this->duration;
}

auto AUTH::Stopwatch::reset() -> void { 
    this->duration = 0;
    this->begin    = 0; 
}

auto AUTH::Stopwatch::restart() -> void { 
    this->reset();
    this->start();
}
