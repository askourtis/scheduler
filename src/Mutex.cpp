#include "Concurrency/Mutex.hpp"
#include "Helpers.hpp"

AUTH::Mutex::Mutex(int const type, int const protocol, int const pshared, int const robustness, int const prioceiling) {
    dynamic_err_assert(pthread_mutexattr_init(&this->attr));

    err_assert(pthread_mutexattr_settype(&this->attr, type));
    err_assert(pthread_mutexattr_setprotocol(&this->attr, protocol));
    err_assert(pthread_mutexattr_setpshared(&this->attr, pshared));
    err_assert(pthread_mutexattr_setrobust(&this->attr, robustness));
    err_assert(pthread_mutexattr_setprioceiling(&this->attr, prioceiling));

    dynamic_err_assert(pthread_mutex_init(&this->cmutex, &this->attr));
}

AUTH::Mutex::~Mutex() {
    dynamic_err_assert(pthread_mutex_destroy(&this->cmutex));
    dynamic_err_assert(pthread_mutexattr_destroy(&this->attr));
}

auto AUTH::Mutex::lock() -> void {
    dynamic_err_assert(pthread_mutex_lock(&this->cmutex));
}

auto AUTH::Mutex::unlock() -> void {
    dynamic_err_assert(pthread_mutex_unlock(&this->cmutex));
}

AUTH::ScopedLock::ScopedLock(Mutex& mutex)
    : mutex(mutex)
{
    this->mutex.lock();
}

AUTH::ScopedLock::~ScopedLock() {
    this->mutex.unlock();
}
