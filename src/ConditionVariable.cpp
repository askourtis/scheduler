#include "Concurrency/ConditionVariable.hpp"
#include "Helpers.hpp"

AUTH::ConditionVariable::ConditionVariable() {
    dynamic_err_assert(pthread_cond_init(&this->ccond, nullptr));
}

AUTH::ConditionVariable::~ConditionVariable() {
    dynamic_err_assert(pthread_cond_destroy(&this->ccond));
}

auto AUTH::ConditionVariable::wait(Mutex& lock, std::function<bool(void)> exitCondition) -> void {
    while (!exitCondition())
        this->wait(lock);
}

auto AUTH::ConditionVariable::wait(Mutex& lock) -> void {
    dynamic_err_assert(pthread_cond_wait(&this->ccond, &lock.cmutex));
}

auto AUTH::ConditionVariable::signal() -> void {
    dynamic_err_assert(pthread_cond_signal(&this->ccond));
}
