#pragma once
#include "BlockingQueue.hpp"

template<class T, class Allocator>
AUTH::BlockingQueue<T, Allocator>::BlockingQueue(Size const capacity)
    : Queue<T, Allocator>(capacity), lock(PTHREAD_MUTEX_RECURSIVE) {}

template<class T, class Allocator>
auto AUTH::BlockingQueue<T, Allocator>::offer(const T& element) -> bool {
    ScopedLock scopedLock(this->lock);
    if (!this->Queue<T, Allocator>::offer(element))
        return false;
    this->notEmpty.signal();
    return true;
}

template<class T, class Allocator>
auto AUTH::BlockingQueue<T, Allocator>::push(const T& element) -> void {
    ScopedLock scopedLock(this->lock);
    this->notFull.wait(this->lock, [&](){ return this->offer(element); });
}

template<class T, class Allocator>
auto AUTH::BlockingQueue<T, Allocator>::pop() -> T {
    ScopedLock scopedLock(this->lock);
    this->notEmpty.wait(this->lock, [&](){ return !this->isEmpty(); });
    this->notFull.signal();
    return this->Queue<T, Allocator>::pop();
}