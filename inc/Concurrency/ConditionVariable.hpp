#pragma once

#include "Mutex.hpp"
#include <functional>

namespace AUTH
{
    using Predicate = std::function<bool(void)>;

    /**
     * @brief A ConditionVariable class
     */
    class ConditionVariable {
    public:
        /**
         * @brief Default constructs the object
         */
        ConditionVariable();

        ConditionVariable(const ConditionVariable&) = delete;
        ConditionVariable(ConditionVariable&&)      = delete;
        
        /**
         * @brief Destroys the object
         */
        virtual ~ConditionVariable();
    
        /**
         * @brief Waits for a the predicate to return true, but checks only when signaled to
         * 
         * @param lock          The mutex to unlock while waiting
         * @param exitCondition The predicate the check
         * 
         * @throw std::runtime_err if any pthread function fails
         */
        auto wait(Mutex& lock, Predicate exitCondition) -> void;

        /**
         * @brief Waits to be signaled
         * 
         * @param lock          The mutex to unlock while waiting
         * 
         * @throw std::runtime_err if any pthread function fails
         */
        auto wait(Mutex& lock) -> void;

        /**
         * @brief Signals the object
         * 
         * @throw std::runtime_err if any pthread function fails
         */
        auto signal() -> void;
    private:
        pthread_cond_t ccond;
    };
}
