#pragma once

#include "Queue.hpp"
#include "ConditionVariable.hpp"

namespace AUTH
{
    /**
     * @brief A generic circular buffer queue that blocks on pop and push if the queue is empty or full
     * 
     * @param T         the type that the queue handles
     * @param Allocator the allocator type
     */
    template<class T, class Allocator = std::allocator<T>>
    class BlockingQueue : public Queue<T, Allocator> {
    protected:
        using Size = typename Queue<T, Allocator>::Size;
    public:
        /**
         * @brief Constructs the queue with a given capacity
         * 
         * @param capacity the queue capacity
         */
        explicit BlockingQueue(Size capacity);

        auto offer(const T& element) -> bool override;
        auto push(const T& element)  -> void override;
        auto pop()                   -> T    override;
    private:
        ConditionVariable notFull, notEmpty;
        Mutex lock;
    };
}

#include "BlockingQueue.tpp"
