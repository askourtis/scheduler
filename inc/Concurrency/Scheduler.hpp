#pragma once

#include <list>
#include "Thread.hpp"
#include "BlockingQueue.hpp"

namespace AUTH
{
    /**
     * @brief A scheduler class schedules repeating tasks to be executed. Allocates a worker pool and a task pool
     */
    class Scheduler {
        using Size    = std::size_t;
        using Time    = Size;
        using Routine = std::function<void(void)>;
    public:
        /**
         * @brief A repeating task to be executed by the scheduler
         */
        class Task {
        public:
            /**
             * @brief Constructs a Task with a given reapeating function
             * 
             * @param timer     The repeating function
             * @param num       The number of times the timer should be executed
             * @param period    The delay between consecutive executions
             * @param delay     The delay before the first execution
             */
            explicit Task(const Routine& timer, Size num = 1, Time period = 0, Time delay = 0);
            /**
             * @brief Constructs a Task with a given reapeating function
             * 
             * @param timer     The repeating function
             * @param start     A function that runs before any timer function
             * @param stop      A function that runs after any timer function
             * @param err       A function that runs if the task pool is full
             * @param num       The number of times the timer should be executed
             * @param period    The delay between consecutive executions
             * @param delay     The delay before the first execution
             */
            explicit Task(const Routine& timer, const Routine& start, const Routine& stop, const Routine& err, Size num = 1, Time period = 0, Time delay = 0);

            /**
             * @brief Executes the start function
             */
            auto start() const -> void;

            /**
             * @brief Executes the timer function
             */
            auto timer() const -> void;

            /**
             * @brief Executes the stop function
             */
            auto stop() const -> void;

            /**
             * @brief Executes the err function
             */
            auto err() const -> void;


            /**
             * @brief Gets the number of times the timer should be executed
             * 
             * @return the number of times the timer should be executed
             */
            auto getNumber() const -> Size;

            /**
             * @brief Gets the delay between consecutive timer executions
             * 
             * @return the delay between consecutive timer executions
             */
            auto getPeriod() const -> Size;

            /**
             * @brief Gets the initial delay
             * 
             * @return the initial delay
             */
            auto getDelay()  const -> Size;
        private:
            Routine const _timer, _start, _stop, _err;
            Size    const num;
            Time    const period, delay;
        };
    private:
        /**
         * @brief Producers offer routines to the task pool if possible
         */
        class Producer : public ScopedWorker {
        public:
            explicit Producer(Scheduler* outer, const Task& task);
        };

        /**
         * @brief Consumers waits for any routine to enter the task pool and executes that routine
         */
        class Consumer : public ScopedWorker {
        public:
            explicit Consumer(Scheduler* outer);
        };
    public:
        /**
         * @brief Constructs a scheduler with the given queue size and pool size
         * 
         * @param queueSize the size of the task queue
         * @param poolSize  the size of the thread pool
         * 
         * @note There is a bug when queueSize < poolSize
         */
        explicit Scheduler(Size queueSize, Size poolSize);

        /**
         * @brief Submits a task to the scheduler for execution
         * 
         * @param task the task
         */
        auto submit(const Task& task) -> void;

        /**
         * @brief Waits for all threads to exit and terminates the scheduler. It ensures that all Producers finished their task
         */
        auto wait() -> void;
    private:
        std::list<Consumer>    consumers;
        std::list<Producer>    producers;
        BlockingQueue<Routine> taskQueue;

        bool terminated;
    };
}
