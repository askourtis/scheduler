#pragma once

#include <pthread.h>

namespace AUTH
{
    /**
     * @brief A mutex class
     */
    class Mutex {
        friend class ConditionVariable;
    public:
        //Section: Constructors/Destructor
        /**
         * @brief Constructs the mutex with the given attributes
         * 
         * @param type          The type of the mutex
         * @param protocol      The protocol of the mutex
         * @param pshared       Whether the mutex is shared across processes
         * @param robustness    The robustness of the mutex
         * @param prioceiling   The priority ceiling of the mutex
         * 
         * @throw std::runtime_error if any pthread functionn fails
         */
        explicit Mutex( int type        = PTHREAD_MUTEX_DEFAULT,
                        int protocol    = PTHREAD_PRIO_NONE,
                        int pshared     = PTHREAD_PROCESS_PRIVATE, 
                        int robustness  = PTHREAD_MUTEX_STALLED, 
                        int prioceiling = PTHREAD_PRIO_INHERIT);
        
        Mutex(const Mutex&) = delete;
        Mutex(Mutex&&)      = delete;

        /**
         * @brief Destroys the object
         * 
         * @throw std::runtime_error if any pthread functionn fails
         */
        virtual ~Mutex();

        //Section: Mutex Behaviour
        /**
         * @brief Locks the mutex if possible or waits until possible
         * 
         * @throw std::runtime_error if any pthread functionn fails
         */
        auto lock() -> void;

        /**
         * @brief Unlocks the mutex
         * 
         * @throw std::runtime_error if any pthread functionn fails
         */
        auto unlock() -> void;
    private:
        pthread_mutexattr_t  attr;
        pthread_mutex_t      cmutex;
    };

    /**
     * @brief Locks a mutex object on construction and unlocks it on destruction
     */
    class ScopedLock {
    public:
        /**
         * @brief Locks the given mutex object
         * 
         * @param mutex The mutex object
         */
        explicit ScopedLock(Mutex& mutex);

        ScopedLock(const ScopedLock&) = delete;
        ScopedLock(ScopedLock&&)      = delete;

        /**
         * @brief Unlocks the handled mutex object
         */
        virtual ~ScopedLock();
    private:
        Mutex& mutex;
    };
}
