#pragma once

#include <pthread.h>
#include <functional>

namespace AUTH
{
    using Runnable = std::function<void()>;

    /**
     * @brief A thread class
     */
    class Thread {
    public:
        //Section: Constructors/Destructor
        /**
         * @brief Deafult constructor
         */
        Thread();

        Thread(const Thread&) = delete;
        Thread(Thread&&)      = delete;

        /**
         * @brief Empty destructor
         */
        virtual ~Thread() = default;

        //Section: Thread Behaviour
        /**
         * @brief Starts the thread
         * 
         * @throw std::runtime_error if the thread is already started
         * @throw std::runtime_error if the thread could not start
         */
        auto start() -> void;

        /**
         * @brief Waits for the thread to stop execution
         */
        auto join() -> void;

        //Section: Getters
        /**
         * @brief Checks if started
         * 
         * @return true if started false otherwise
         */
        auto isStarted() const -> bool;

        /**
         * @brief Checks if joined
         * 
         * @return true if joined false otherwise
         */
        auto isJoined() const -> bool;
    protected:
        /**
         * @brief The task of the thread
         */
        virtual auto task() const -> void = 0;
    private:
        bool started, joined;
        pthread_t cthread;
    };

    /**
     * @brief A worker class
     */
    class Worker : public Thread {
    public:
        /**
         * @brief Constructs the worker with a given task
         * 
         * @param runnable The task to execute
         */
        Worker(const Runnable& runnable);
    protected:
        auto task() const -> void override;
    private:
        Runnable const runnable;
    };

    /**
     * @brief a worker that starts on construction and joins on deletion
     */
    class ScopedWorker : public Worker {
    public:
        /**
         * @brief Constructs the worker with a given task, and starts the worker
         * 
         * @param runnable The task to execute
         */
        ScopedWorker(const Runnable& runnable);

        /** @brief Joins the worker before destroying the object */
        ~ScopedWorker();
    };
}
