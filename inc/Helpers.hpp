#pragma once

/**
 * @brief Checks the error code given and throws if the error code is not zero
 * 
 * @param err The error code
 * 
 * @throw std::runtime_error if the error code is not zero
 */
#define dynamic_err_assert(EXPR) _dynamic_err_assert(EXPR, #EXPR, __FILE__, __LINE__)
#ifndef NDEBUG
#define err_assert(EXPR)         _dynamic_err_assert(EXPR, #EXPR, __FILE__, __LINE__)
#else
#define err_assert(EXPR)         EXPR
#endif

/**
 * @brief Checks the error code given and throws if the error code is false
 * 
 * @param err The error code
 * 
 * @throw std::runtime_error if the error code is not zero
 */
#define dynamic_assert(EXPR) _dynamic_assert(EXPR, #EXPR, __FILE__, __LINE__)
#ifndef NDEBUG
#define assert(EXPR)         _dynamic_assert(EXPR, #EXPR, __FILE__, __LINE__)
#else
#define assert(EXPR)         EXPR
#endif

namespace AUTH
{
    auto _dynamic_err_assert(int err, const char* sig, const char* file, int line) -> void;
    auto _dynamic_assert(int err, const char* sig, const char* file, int line) -> void;
}

