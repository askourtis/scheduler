#pragma once

#include <cstddef>  //Need: std::size_t
#include <memory>   //Need: std::allocator

namespace AUTH
{
    /**
     * @brief A generic circular buffer queue
     * 
     * @param T         the type that the queue handles
     * @param Allocator the allocator type
     */
    template<class T, class Allocator = std::allocator<T>>
    class Queue {
    protected:
        /** @brief The size type of the queue */
        using Size = std::size_t;
    public:
        //Section: Constructors/Destructor
        /**
         * @brief Constructs the queue with a given capacity
         * 
         * @param capacity the queue capacity
         */
        explicit Queue(Size capacity);

        /**
         * @brief Copy constructs the queue
         * 
         * @param other the other queue
         */
        Queue(const Queue& other);

        /**
         * @brief Move constructs the queue
         * 
         * @param other the other queue
         */
        Queue(Queue&& other);

        /** @brief destroys the queue */
        virtual ~Queue();

        //Section: Queue behaviour
        /**
         * @brief Offers an element to the queue
         * 
         * @param element the element to offer
         * 
         * @return true if the queue accepts the element, false otherwise
         */
        virtual auto offer(const T& element) -> bool;

        /**
         * @brief Pushes an element to the queue
         * 
         * @param element the element to push
         * 
         * @throw std::runtime_error if the queue is full
         */
        virtual auto push(const T& element) -> void;

        /**
         * @brief Pops the last element of the queue
         * 
         * @return the last element
         * 
         * @throw std::runtime_error if the queue is empty
         */
        virtual auto pop() -> T;

        //Section: Getters
        /**
         * @brief Gets the current size of the queue
         * 
         * @return the current size
         */
        auto getSize() const -> Size;

        /**
         * @brief Gets the capacity of the queue
         * 
         * @return the capacity
         */
        auto getCapacity() const -> Size;

        /**
         * @brief Checks if the queue is full
         * 
         * @return true if the queue is full, false otherwise
         */
        auto isFull() const -> bool;

        /**
         * @brief Checks if the queue is empty
         * 
         * @return true if the queue is empty, false otherwise
         */
        auto isEmpty() const -> bool;
    private:
        Allocator allocator;

        Size capacity, size, head, tail;
        T*   data;
    };
}

#include "Queue.tpp"
