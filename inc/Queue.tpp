#pragma once
#include "Queue.hpp"
#include <stdexcept>
#include <cstring>

template<class T, class Allocator>
AUTH::Queue<T, Allocator>::Queue(Size const capacity)
    : allocator(), size(0), head(0), tail(0), capacity(capacity), data(allocator.allocate(capacity)) {}

template<class T, class Allocator>
AUTH::Queue<T, Allocator>::Queue(const Queue& other)
    : allocator(other.allocator), size(other.size), head(other.head), tail(other.tail), capacity(other.capacity), data(static_cast<decltype(this->data)>(memcpy(allocator.allocate(capacity), other.data, this->capacity*sizeof(T)))) {}

template<class T, class Allocator>
AUTH::Queue<T, Allocator>::Queue(Queue&& other)
    : allocator(std::move(other.allocator)), size(other.size), head(other.head), tail(other.tail), capacity(other.capacity), data(other.data)
{
    other.data = nullptr;
    other.capacity = 0;
    other.size = 0;
}

template<class T, class Allocator>
AUTH::Queue<T, Allocator>::~Queue() {
    for(Size i = 0; i < this->size; ++i)
        this->allocator.destroy(&this->data[i]);
    this->allocator.deallocate(this->data, this->capacity);
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::offer(const T& element) -> bool {
    if(this->isFull())
        return false;
    this->data[this->tail] = element;
    this->tail = (this->tail + 1) % this->capacity;
    this->size++;
    return true;
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::push(const T& element) -> void {
    if(!this->offer(element))
        throw std::runtime_error("Queue was full");
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::pop() -> T {
    if(this->isEmpty())
        throw std::runtime_error("Queue was empty");
    
    T& element = this->data[this->head];
    this->head = (this->head + 1) % this->capacity;
    this->size--;
    return std::move(element);
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::getSize() const -> AUTH::Queue<T, Allocator>::Size {
    return this->size;
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::getCapacity() const -> AUTH::Queue<T, Allocator>::Size {
    return this->capacity;
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::isFull() const -> bool {
    return this->size == this->capacity;
}

template<class T, class Allocator>
auto AUTH::Queue<T, Allocator>::isEmpty() const -> bool {
    return this->size == 0;
}
