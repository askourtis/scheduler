#pragma once

#include <sys/time.h>

namespace AUTH
{
    using Time = unsigned long;

    /**
     * @brief A simple Stopwatch implementation
     */
    class Stopwatch {
        /**
         * @brief Gets the current istance of the wall clock in microseconds
         * 
         * @return the curent instance
         */
        static auto Clock() -> Time;
    public:
        /**
         * @brief Constructs the stopwatch and either starts it or not
         * 
         * @param start If the constructor should start or not the stopwatch
         */
        Stopwatch(bool start = true);
        
        /**
         * @brief Starts the stopwatch. If the stopwatch is already started, then no operation takes place
         */
        auto start() -> void;

        /**
         * @brief Stops the stopwatch. If the stopwatch is already stopped, then no operation takes place
         */
        auto stop() -> void;

        /**
         * @brief Checks if the stopwatch is counting (started)
         */
        auto isCounting() -> bool;

        /**
         * @brief Gets the current duration of the stopwatch
         */
        auto getDuration() -> Time;

        /**
         * @brief Resets the the stopwatch
         */
        auto reset() -> void;

        /**
         * @brief Resets and starts the stopwatch
         */
        auto restart() -> void;
    private:
        Time begin, duration;
    };
}
