
CC := g++

SRC_DIR := ./src
INC_DIR := ./inc
OBJ_DIR := ./obj
BLD_DIR := ./build

EXECUTABLE := $(BLD_DIR)/runnable.a


CFLAGS := -I$(INC_DIR) -g3
LFLAGS := -lpthread


SOURCES := $(wildcard $(SRC_DIR)/*.cpp)
OBJECTS := $(patsubst $(SRC_DIR)/%.cpp, $(OBJ_DIR)/%.o, $(SOURCES))



all: purge_compile

clean_compile: clean compile
purge_compile: purge compile

compile: $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $(EXECUTABLE) $(LFLAGS)

#Compile each
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp mkdirs
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: mkdirs
mkdirs:
	mkdir -p $(OBJ_DIR)
	mkdir -p $(BLD_DIR)

.PHONY: clean
clean:
	rm -rf $(OBJ_DIR)/*

.PHONY: purge
purge: clean
	rm -rf $(BLD_DIR)/*